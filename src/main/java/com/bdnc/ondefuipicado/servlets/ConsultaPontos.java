/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bdnc.ondefuipicado.servlets;

import com.bdnc.ondefuipicado.entidades.Foco;
import com.bdnc.ondefuipicado.servicos.ServicosFoco;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Marcelo Augusto
 */
public class ConsultaPontos extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ServicosFoco servicosFoco = new ServicosFoco();
        List<Foco> focos = servicosFoco.buscarFocos();
        request.getSession().setAttribute("focos", focos);
        request.getSession().setAttribute("pointsLoaded", true);
        response.sendRedirect("index.jsp");
    }

    
}
