
package com.bdnc.ondefuipicado.servlets;

import com.bdnc.ondefuipicado.entidades.Usuario;
import com.bdnc.ondefuipicado.servicos.ServicosUsuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Marcelo Augusto
 */
public class CadastraUsuarios extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

  
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Usuario usuario = new Usuario();
        usuario.setNome(request.getParameter("nome"));
        usuario.setEmail(request.getParameter("email"));
        usuario.setSenha(request.getParameter("senha"));
        ServicosUsuario servicosUsuario = new ServicosUsuario();
        servicosUsuario.salvar(usuario);
        response.sendRedirect("novo-foco.jsp");
    }

}
