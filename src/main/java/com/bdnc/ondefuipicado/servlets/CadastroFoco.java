/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bdnc.ondefuipicado.servlets;

import com.bdnc.ondefuipicado.entidades.Foco;
import com.bdnc.ondefuipicado.servicos.ServicosFoco;
import com.bdnc.ondefuipicado.servicos.ServicosUsuario;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Marcelo Augusto
 */
public class CadastroFoco extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String lat = request.getParameter("latitude");
        String lgt = request.getParameter("longitude");
        String doenca = request.getParameter("doenca");
        String data = request.getParameter("data");
        String descricao = request.getParameter("descricao");
        ServicosFoco servicosFoco = new ServicosFoco();
        Foco foco = new Foco();
        foco.setDescricao(descricao);
        foco.setData(servicosFoco.converterParaDate(data));
        foco.setDoenca(doenca);
        try {
            WKTReader reader = new WKTReader();
            Geometry ponto = reader.read("POINT("+lat+" "+ lgt+")");
            foco.setLocalizacao(ponto);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        servicosFoco.salvarFoco(foco);
        response.sendRedirect("ConsultaPontos");
    }

}
