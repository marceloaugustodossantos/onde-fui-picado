/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bdnc.ondefuipicado.servlets;

import com.bdnc.ondefuipicado.entidades.Usuario;
import com.bdnc.ondefuipicado.servicos.ServicosUsuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Marcelo Augusto
 */
public class Login extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String senha = request.getParameter("senha");
        ServicosUsuario servicosUsuario = new ServicosUsuario();
        Usuario usuario = servicosUsuario.buscarPorEmail(email);
        if (usuario != null) {
            if (senha.equals(usuario.getSenha())) {
                request.getSession().setAttribute("usuario", usuario);
                response.sendRedirect("novo-foco.jsp");
            }
            else {
                response.sendRedirect("login.html");
            }
        }else{
            response.sendRedirect("login.html");
        }

    }

}
