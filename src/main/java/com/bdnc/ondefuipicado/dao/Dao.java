/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bdnc.ondefuipicado.dao;

import com.bdnc.ondefuipicado.entidades.Foco;
import com.bdnc.ondefuipicado.entidades.Usuario;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.WKTReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Statement;
import java.util.ArrayList;import java.util.Iterator;
import java.util.List;
;

/**
 *
 * @author Marcelo Augusto
 */
public class Dao {
    
    Conexao conexao = new Conexao(); 
    
    
    public void salvarUsuario(Usuario usuario){
        String sql = "INSERT INTO USUARIO (nome, senha, email) VALUES (?, ?, ?)";
        try {
            Connection connection = conexao.getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, usuario.getNome());
            statement.setString(2, usuario.getSenha());
            statement.setString(3, usuario.getEmail());
            statement.execute();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public Usuario bucarPorEmail(String email){
        String sql = "SELECT * FROM usuario WHERE email ='" + email + "'";
        Usuario usuario = null;
        try {
            Connection connection = conexao.getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            if (rs.next()) {
                usuario = new Usuario();
                usuario.setId(rs.getInt("id"));
                usuario.setNome(rs.getString("nome"));
                usuario.setEmail(rs.getString("email"));
                usuario.setSenha(rs.getString("senha"));
            }
            statement.close();
            connection.close();
            return usuario;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return usuario;
    }
    
    public void salvarFoco(Foco foco){
        String sql = "INSERT INTO FOCO (descricao, data, doenca, ponto) VALUES "
                + "(?, ?, ?, st_geomFromText('Point("+foco.getLocalizacao().getCoordinate().x +" "
                + foco.getLocalizacao().getCoordinate().y +")'))";
        try {
            Connection connection = conexao.getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, foco.getDescricao());
            Date dateSql = new Date(foco.getData().getTime());
            statement.setDate(2, dateSql);
            statement.setString(3, foco.getDoenca());
            statement.execute();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public List<Foco> consultarPontos(){
        List<Foco> focos = new ArrayList<>();
        String sql = "SELECT ST_AsText(f.ponto) ponto, f.id, f.descricao, f.data, f.doenca FROM foco f";
        try {
            Connection connection = conexao.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Foco foco = new Foco();
                foco.setId(resultSet.getInt("id"));
                foco.setDescricao(resultSet.getString("descricao"));
                foco.setDoenca(resultSet.getString("doenca"));
                foco.setData(new java.util.Date(resultSet.getDate("data").getTime()));
                WKTReader reader = new WKTReader();
                Geometry geometry = reader.read(resultSet.getString("ponto"));                
                foco.setLocalizacao(geometry);
                foco.setLat(geometry.getCoordinate().x);
                foco.setLgt(geometry.getCoordinate().y);
                focos.add(foco);
            }
            statement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return focos;
    }
    
}
