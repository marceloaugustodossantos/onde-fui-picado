/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bdnc.ondefuipicado.dao;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Marcelo Augusto
 */
public class Conexao {
    private String url = "jdbc:postgresql://localhost:5432/bdncgeo";
    private String usuario = "postgres";
    private String senha = "123456";
    
    public Connection getConnection() throws Exception{
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager.getConnection(url, usuario, senha);
            return c;
        }
        catch(Exception e){
            throw new Exception();
        }
    }
}
