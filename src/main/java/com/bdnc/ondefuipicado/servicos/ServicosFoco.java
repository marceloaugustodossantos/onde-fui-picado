/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bdnc.ondefuipicado.servicos;

import com.bdnc.ondefuipicado.dao.Dao;
import com.bdnc.ondefuipicado.entidades.Foco;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Marcelo Augusto
 */
public class ServicosFoco {
    
    
    Dao dao = new Dao();
    
    public void salvarFoco(Foco foco){
        dao.salvarFoco(foco);
    }
    
    public List<Foco> buscarFocos(){
        return dao.consultarPontos();
    }    
    
    public Date converterParaDate(String date){
        Date data = new Date();
        int ano = Integer.parseInt(date.substring(0, 4));
        int mes = Integer.parseInt(date.substring(5, 7));
        data.setYear(ano);
        data.setMonth(mes);
        return data;
    }
    
}
