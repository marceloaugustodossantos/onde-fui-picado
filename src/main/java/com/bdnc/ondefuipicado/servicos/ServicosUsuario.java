/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bdnc.ondefuipicado.servicos;


import com.bdnc.ondefuipicado.dao.Dao;
import com.bdnc.ondefuipicado.entidades.Foco;
import com.bdnc.ondefuipicado.entidades.Usuario;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Marcelo Augusto
 */
public class ServicosUsuario {
    
    Dao dao = new Dao();
    
    public void salvar(Usuario usuario){
        dao.salvarUsuario(usuario);
    }
    
    public Usuario buscarPorEmail(String email){
        return dao.bucarPorEmail(email);
    }
    
}
