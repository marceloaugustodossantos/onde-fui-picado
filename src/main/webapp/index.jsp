<%-- 
    Document   : index
    Created on : 18/01/2016, 10:12:14
    Author     : Marcelo Augusto
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.bdnc.ondefuipicado.entidades.Foco"%>
<%@page import="java.lang.String"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head lang="pt-br">
        <title>Onde fui picado</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link  href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link  href="css/style.css" rel="stylesheet" type="text/css">
        <script src="js/jquery.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>  
        <script>
            var heatmap, map;
            var markers = [];
            function initMap() {
                map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: -6.887828, lng: -38.559485},
                    zoom: 14
                });


                var infoWindow = new google.maps.InfoWindow({map: map});

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        map.setCenter(pos);
                    }, function () {
                        handleLocationError(true, infoWindow, map.getCenter());
                    });
                } else {
                    // Browser doesn't support Geolocation
                    handleLocationError(false, infoWindow, map.getCenter());
                }

                var input = /** @type {!HTMLInputElement} */(document.getElementById('pac-input'));

                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.bindTo('bounds', map);
                
                autocomplete.addListener('place_changed', function () {
                    infoWindow.close();
                   
                    var place = autocomplete.getPlace();
                    if (!place.geometry) {
                        window.alert("Autocomplete's returned place contains no geometry");
                        return;
                    }

                    // If the place has a geometry, then present it on a map.
                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(17);  // Why 17? Because it looks good.
                    }

                    var address = '';
                    if (place.address_components) {
                        address = [
                            (place.address_components[0] && place.address_components[0].short_name || ''),
                            (place.address_components[1] && place.address_components[1].short_name || ''),
                            (place.address_components[2] && place.address_components[2].short_name || '')
                        ].join(' ');
                    }
                });

                alterarParaMapaNormal(map);

            }

            function alterarParaHeatMap() {
                heatmap = new google.maps.visualization.HeatmapLayer({
                    data: getPoints(),
                    map: map
                });
                heatmap.set('radius', heatmap.get('radius') ? null : 20);
                setMapOnAll(null);
            }

            function alterarParaMapaNormal() {
                if (heatmap) {
                    heatmap.setMap(heatmap.getMap() ? null : map);
                }
                addMarker(map);
            }

            function addMarker(map) {
            <c:forEach items="${focos}" var="f">
                var marker = new google.maps.Marker({
                    position: {lat: ${f.lat}, lng: ${f.lgt}},
                    map: map,
                    icon: { 
                    <c:choose>
                        <c:when test="${f.doenca == 'dengue'}"> 
                            url: 'imagens/ponto-preto.png',
                            size: new google.maps.Size(23, 38),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(0, 38)
                        </c:when>
                        <c:when test="${f.doenca == 'zika'}"> 
                            url: 'imagens/ponto-azul-p.jpg',
                            size: new google.maps.Size(23, 38),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(0, 38)
                        </c:when>
                        <c:when test="${f.doenca == 'chikungunya'}"> 
                            url: 'imagens/ponto-verde-p.jpg',
                            size: new google.maps.Size(23, 38),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(0, 38)
                        </c:when>
                        <c:when test="${f.doenca == 'febre-amarela'}"> 
                            url: 'imagens/ponto-amarelo-p.jpg',
                            size: new google.maps.Size(23, 38),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(0, 38)
                        </c:when>
                    </c:choose>
                    }                    
                });
                markers.push(marker);
            </c:forEach>
            }

            function setMapOnAll(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            function getPoints() {
                return [
            <c:forEach items="${focos}" var="f">
                    new google.maps.LatLng(${f.lat}, ${f.lgt}),
            </c:forEach>
                    new google.maps.LatLng(0, 0)
                ];
            }

            google.maps.event.addDomListener(window, 'load', initialize);
            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
            }
        </script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqAOF3CcILJ6MwkGxW0DvojCwgokbAe5E&signed_in=true&libraries=places,visualization&callback=initMap&language=br&region=br"
                async defer>
        </script>

    </head>
    <body>
        <header id="index" class="container">
            <div id="cabecalhoIndex" class="row">
                <div id="logo" class="col-md-6">                
                    <b class="h2" style="margin-left: 30%; position: relative; top: 10px; color: #f9f2f4">
                        <img height="40" src="imagens/medium_icone.png"/> Onde fui picado
                    </b>
                </div>
                <div class="col-md-3">
                    <a href="cadastro.html" class="h4" style="position: relative; top: 25px; left: 80%;color: #f9f2f4">Cadastrar-se</a>
                </div>
                <div   class="col-md-3"  >
                    <ul class="nav navbar-nav" style="position: relative; top: 8px; left: 25px">
                        <li class="dropdown">
                            <a href="#" id="btOpt" style="color: #f9f2f4; font-size: 18px;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Login</a>
                            <ul class="dropdown-menu">
                                <li><a href="login.html">Entrar</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="Logout">Sair</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

        </header>
        <input id="pac-input" class="controls" type="text" placeholder="Digite o endereço">
        <c:choose>
            <c:when test="${pointsLoaded == null}">
                <div id="imagemIndex">
                    <a href="ConsultaPontos" class="btn btn-info btn-lg">
                        <img src="imagens/mosquito.png" height="50" width="50" style="border-radius: 20%" ><b style="font-size: 20px">Veja focos na sua cidade</b>
                    </a>
                </div>
                </c:when>
                <c:when test="${pointsLoaded == true}">
                <div id="map"></div>
            </c:when>
        </c:choose>
        <div id="conteudoLateral">
            <h2>Veja a incidência de focos do mosquito na sua cidade</h2>
            <a href="VerificaUsuario" id="btMarcarFoco" class="btn btn-success btn-lg">Marque um foco agora</a>
            <a href="#" onclick="alterarParaHeatMap()" class="btn btn-default" id="btMapaDeCalor">
                <img src="imagens/heatmap-400.png" height="23" width="23"><b>Mapa de calor</b>
            </a>
            <a href="#" onclick="alterarParaMapaNormal()"class="btn btn-default" id="btMapaNormal">Mapa normal</a>
            <div id="legenda">
                <h4>Legenda</h4>
                <img src="imagens/ponto-preto.png" height="25" width="18">   <i> ------ Dengue</i><br>
                <img src="imagens/ponto-azul-p.jpg"height="25" width="18">   <i> ------ Zika</i><br>
                <img src="imagens/ponto-amarelo-p.jpg"height="25" width="18"><i> ------ Febre amarela</i><br>
                <img src="imagens/ponto-verde-p.jpg"height="25" width="18">  <i> ------ Chikungunya</i><br>
                <img src="imagens/red-icon.png"height="25" width="18">       <i> ------ Apenas o foco</i><br>
            </div>
        </div>
    </body>
</html>
