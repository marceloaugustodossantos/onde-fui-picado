<%-- 
    Document   : novo-foco
    Created on : 15/01/2016, 16:42:11
    Author     : Marcelo Augusto
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head lang="pt-br">
        <title>Onde fui picado</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link  href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link  href="css/style-novo-foco.css" rel="stylesheet" type="text/css">
        <script src="js/jquery.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>  
        <script>
            var marker;
            function initMap() {
                var map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: -6.887828, lng: -38.559485},
                    zoom: 14
                });
                var infoWindow = new google.maps.InfoWindow({map: map});

                google.maps.event.addListener(map, 'click', function (event) {
                    addMarker(event.latLng, map);
                });

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        map.setCenter(pos);
                    }, function () {
                        handleLocationError(true, infoWindow, map.getCenter());
                    });
                } else {
                    // Browser doesn't support Geolocation
                    handleLocationError(false, infoWindow, map.getCenter());
                }

                var input = /** @type {!HTMLInputElement} */(document.getElementById('pac-input'));

                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.bindTo('bounds', map);

                autocomplete.addListener('place_changed', function () {
                    infoWindow.close();
                    var place = autocomplete.getPlace();
                    if (!place.geometry) {
                        window.alert("O endereço informado não foi encontrado!");
                        return;
                    }

                    // If the place has a geometry, then present it on a map.
                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(17);  // Why 17? Because it looks good.
                    }

                    var address = '';
                    if (place.address_components) {
                        address = [
                            (place.address_components[0] && place.address_components[0].short_name || ''),
                            (place.address_components[1] && place.address_components[1].short_name || ''),
                            (place.address_components[2] && place.address_components[2].short_name || '')
                        ].join(' ');
                    }
                });
            }

            // Adds a marker to the map.
            function addMarker(location, map) {
                if (marker) {
                    // remover esse marcador do mapa
                    marker.setMap(null);
                    // remover qualquer valor da variável marker
                    marker = "";
                }

                marker = new google.maps.Marker({
                    position: location,
                    draggable: true,
                    map: map
                });

                // Evento que detecta o arrastar do marcador para redefinir as coordenadas lat e lng
                google.maps.event.addListener(marker, 'dragend', function () {
                    // Actualiza as coordenadas de posição do marcador no mapa
                    marker.position = marker.getPosition();
                    var lat = marker.position.lat().toFixed(6);
                    var lng = marker.position.lng().toFixed(6);
                    getCoords(lat, lng);
                });
                var lat = marker.position.lat().toFixed(6);
                var lng = marker.position.lng().toFixed(6);
                getCoords(lat, lng);
            }

            function getCoords(lat, lng) {
                var coords_lat = document.getElementById('lat');
                coords_lat.value = lat;
                var coords_lng = document.getElementById('lng');
                coords_lng.value = lng;
            }

            google.maps.event.addDomListener(window, 'load', initialize);

            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqAOF3CcILJ6MwkGxW0DvojCwgokbAe5E&signed_in=true&libraries=places&callback=initMap&language=br&region=br"
                async defer>
        </script>
    </head>
    <body>
        <header id="index" class="container">
            <div id="cabecalhoIndex" class="row">
                <div id="logo" class="col-md-6">                
                    <a href="index.jsp">
                        <b class="h2" style="margin-left: 30%; position: relative; top: 10px; color: #f9f2f4">
                            <img height="40" src="imagens/medium_icone.png"/> Onde fui picado
                        </b>
                    </a>
                </div>
                <div class="col-md-3">
                    
                </div>
                <div   class="col-md-3" >
                    <a href="Logout" class="h4" style="position: relative; top: 25px; left: 20%;color: #f9f2f4">Sair</a>
                </div>
            </div>
        </header>

        <div class="container">
            <div class="row">
                <div id="conteudo-superior">
                    <h3>Informe o endereço onde você encotrou o foco do mosquito</h3>
                </div>
                <input id="pac-input" class="controls" type="text" placeholder="Digite o endereço">
                <div id="map"></div>
            </div>
            <div class="row">
                <div id="informacoes-extras">
                    <form class="form-horizontal" action="CadastroFoco">
                        <input type="hidden" name="latitude" id="lat">
                        <input type="hidden" name="longitude" id="lng">
                        <h4 style="margin-bottom: 2%">Dados extras</h4>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label for="tipo" >Doença</label>
                                <select name="doenca" class="form-control"id="tipo">
                                    <option value="nenhuma">não fui picado</option>
                                    <option value="dengue">dengue</option>
                                    <option value="zika">zika</option>
                                    <option value="chikungunya">chikungunya</option>
                                    <option value="febre-amarela">febre amarela</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label for="data">Data</label>
                                <input type="month"  class="form-control" name="data"id="data" placeholder="Data">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <textarea class="form-control" name="descricao" rows="3" placeholder="Fale mais sobre o caso que você está registrando"></textarea>
                            </div>
                            <div class="col-sm-6">
                                <button type="submit" style="width: 200px; margin-left: 150px" class="btn btn-success btn-lg">Registrar foco</button>
                            </div>
                        </div>
                    </form><br>
                </div>
            </div>
        </div>
        <footer>
            <br>
            <b>By Marcelo Augusto &copy; </b>
        </footer>
    </body>

</html>

